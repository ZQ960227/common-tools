﻿using DotNet.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kuiyu.MvcWebAPI.Controllers
{
    public class HomeController : Controller
    {
        public ContentResult Index()
        {
            string html = "<div class='clear' style='width: 1000px; height: 1000px; color: red; '>";
             html += new BarCodeToHTML().get39("432432423242", 5,137);
            html += "</div>";
            return Content(html);
        }
    }
}
